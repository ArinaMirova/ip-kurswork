(function(){
    var tmp = '<div class="message {class}">\
            <div class="text">{message}</div>\
            <div class="time-day">{date-d}</div>\
            <div class="time-hour">{date-h}</div>\
        </div>';
    
    var count = 0;
    
    var $form = $('#ms-sent');
    var $msgs = $('#messages-list');
    
    setInterval(UpdateMessages, 1000);
    
    $form.submit(function(e){
        var url = '/messages/add';
        
        $.ajax({
            type: 'POST',
            url: url,
            data: $form.serialize(),
            success: function(data) {
                UpdateMessages();
                
                $form[0].reset();
            }
        });
        
        e.preventDefault();
    });
    
    function UpdateMessages() {        
        var url = '/messages/get';
        
        $.ajax({
            type: 'POST',
            url: url,
            data: $form.serialize(),
            success: function(data) {
                arr = JSON.parse(data);
                var c = 0;
                $msgs.html('');
                
                for(var i = 0; i < arr.length; i++) {   
                    c++;
                    
                    var date = new Date(arr[i]['date']);
                    var date_d = date.getFullYear() + '-' + (date.getMonth() > 9 ? '' : '0') + date.getMonth() + '-' + (date.getDate() > 9 ? '' : '0') + date.getDate();
                    var date_h = (date.getHours() > 9 ? '' : '0') + date.getHours() + ':'+ (date.getMinutes() > 9 ? '' : '0') + date.getMinutes();
                    
                    var html = tmp.replace('{class}', ($('#author').val() == arr[i]['author_id'] ? 'right' : 'left'))
                                  .replace('{message}', arr[i]['message'])
                                  .replace('{date-d}', date_d)
                                  .replace('{date-h}', date_h);
                    
                    $msgs.append(html);
                }
                
                if(count != c) {
                    $msgs.scrollTop($msgs[0].scrollHeight);
                    
                    count = c;
                }
            }
        });
    }
})();