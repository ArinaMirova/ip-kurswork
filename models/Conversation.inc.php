<?php
class Conversation {
    public $id, $user, $message;
    
    public function __construct($data) {
        $this->id = $data['id'];
    }
    
    public function AddUser($user) {
        $this->user = $user;
    }
    
    public function AddMessage($message) {
        $this->message = $message;
    }
}