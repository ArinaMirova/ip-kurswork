<?php
class Message {
    public $id, $author_id, $conversation_id, $message, $date;
    
    public function __construct($data) {
        $this->id = $data['id'];
        $this->author_id = $data['author_id'];
        $this->conversation_id = $data['conversation_id'];
        $this->message = $data['message'];
        $this->date = $data['date'];
    }
}