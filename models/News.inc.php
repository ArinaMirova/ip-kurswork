<?php
class News {
    public $id, $title, $content, $author, $date, $isEdited;
    
    public function __construct($data) {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->content = $data['content'];
        $this->author = $data['author'];
        $this->date = $data['date'];
        $this->isEdited = $data['isEdited'];
    }
}