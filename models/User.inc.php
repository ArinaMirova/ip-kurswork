<?php
class User {
    public $id, $email, $password, $name, $surname, $info;
    
    public function __construct($data) {
        $this->id = $data['id'];
        $this->email = $data['email'];
        $this->password = $data['password'];
        $this->name = $data['name'];
        $this->surname = $data['surname'];
    }
    
    public function AddInfo($row) {
        $this->info = new UserInfo($row);
    }
}