<?php
class UserInfo {
    public $id, $biography, $intereses, $adress, $birthday, $user_id;
    
    public function __construct($data) {
        $this->id = $data['id'];
        $this->biography = $data['biography'];
        $this->intereses = $data['intereses'];
        $this->adress = $data['adress'];
        $this->birthday = $data['birthday'];
        $this->user_id = $data['user_id'];
    }
}