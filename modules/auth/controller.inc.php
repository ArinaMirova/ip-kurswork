<?php
class Auth_Controller
{
    public function IndexAction() {
        $user = Auth::GetUser();
        
        Core::ReturnView(
            array(
                "PageTitle" => "Інформація про користувача",
                "Content" => Auth_View::Index($user)
            )
        );
    }
    public function LoginAction() {        
        if($_SERVER['REQUEST_METHOD'] == "POST" && !isset($_SESSION['logedin'])) {
            $data = $_POST;
            
            Auth::Login($data['email'], $data['password']);
            
            Core::Redirect('news');
        }
        
        Core::ReturnView(
            array(
                "PageTitle" => "Увійти",
                "Content" => Auth_View::Login()
            )
        );
    }
    public function LogoutAction() {
        Auth::Logout();
        
        Core::Redirect('news');
    }
    public function RegisterAction()
    {
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $row = $_POST;
            Auth::Register($row);
            Auth::Login($row['email'], $row['password']);
            
            Core::Redirect('news');
        }
        
        Core::ReturnView(
            array(
                "PageTitle" => "Реєстрація",
                "PageHeaderTitle" => "Реєстрація",
                "Content" => Auth_View::Register()
            )
        );
    }
}