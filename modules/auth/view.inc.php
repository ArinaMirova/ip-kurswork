<?php
class Auth_View
{
    public static function Index($user)
    {
        $tpl = new Template('template/auth/index.tpl');
        if($user) {            
            $tpl->SetParam('user', $user);
        }
        return $tpl->GetHTML();
    }
    public static function Register()
    {
        $tpl = new Template('template/auth/register.tpl');
        return $tpl->GetHTML();
    }
    public static function Login()
    {
        $tpl = new Template('template/auth/login.tpl');
        return $tpl->GetHTML();        
    }
}