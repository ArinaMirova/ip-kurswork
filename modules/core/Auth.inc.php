<?php
class Auth{
    private static $salt = 'Gms532AsF';
    private static $user;
    
    public static function CheckUserExists($email) {
        $userId = Core::$Db->Select('user', ['id'], ['email' => $email]);
        return !!$userId;
    }
    
    public static function CheckLoginValid($email, $password) {
        $userId = Core::$Db->Select('user', ['id'], ['email' => $email]);
                
        if(!$userId) {
            return false;
        } else {
            $passHash = Core::$Db->Select('user', ['password'], ['id' => $userId['id']])['password'];
                        
            return $passHash == md5($password . self::$salt);
        }
    }
    
    public static function Login($email, $password) {
        if(self::CheckLoginValid($email, $password)) {
            $_SESSION['loggedin'] = true;
            
            self::$user = new User(Core::$Db->Select('user','*', ['email' => $email]));
            
            $_SESSION['userId'] = self::$user->id;
        } else {
            throw new Error('Not valid user name or email');
        }
    }
    
    public static function IsAuthorized() 
    {
        return isset($_SESSION['loggedin']) && isset($_SESSION['userId']);
    }
    
    public static function Register($row) {
        if(!self::CheckUserExists($row['email'])) {
            $row['password'] = md5($row['password'] . self::$salt);
            Core::$Db->Insert('user', $row); 
        } else {            
            throw new Error('User already exists');
        }
    }
    
    public static function GetUser() 
    {
        if(self::IsAuthorized()) {
            if(!self::$user) {
                self::$user = new User(Core::$Db->Select('user','*', ['id' => $_SESSION['userId']]));
            }
            
            return self::$user;
        } else {
            self::Logout();
            
            return null;
        }
    }
    
    public static function Logout()
    {
        session_destroy();
        session_start();
        
        self::$user = null;
    }
}