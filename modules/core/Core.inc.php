<?php
class Core
{
    public static $Db;
    public static $IndexTPL;
    
    private static $controller;
    private static $method;
    
    public static function GetUrlData() {
        return array(
            'protocol' => (isset($_SERVER['HTTPS']) ? "https" : "http"),
            'host' => $_SERVER['HTTP_HOST'],
            'request' => $_SERVER['REQUEST_URI'],
            'full' => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
        );
    }
    public static function Init()
    {
        session_start();
        self::$Db = new Database("localhost", 'cms', 'root', '');
        self::$IndexTPL = new Template("template/layout.tpl");
    }
    public static function Run()
    {
        $url = $_GET['url'];
        $parts = explode('/', $url);
        $controllerName = ucfirst(array_shift($parts));
        $actionName = ucfirst(array_shift($parts));
        
        if(empty($controllerName)) {
            self::Redirect('news'); 
        } else {
            self::$controller = $controllerName;
            
            $className = $controllerName.'_Controller';
        
            if (class_exists($className))
            {
                $moduleObject = new $className();

                if(empty($actionName)) {
                    if (method_exists($moduleObject, "IndexAction")) {
                        self::$method = 'index';
                        
                        $moduleObject->IndexAction($parts);   
                    } else {
                        self::Redirect('errors', 'notfound');        
                    }
                } else {
                    self::$method = $actionName;
                    
                    $methodName = $actionName.'Action';

                    if (method_exists($moduleObject, $methodName))
                    {
                        $moduleObject->$methodName($parts);
                    }
                    else
                    {
                        self::Redirect('errors', 'notfound');
                    }
                }
            } else
            {
                self::Redirect('errors', 'notfound');
            }
        }
    }
    public static function ReturnView($params) {
        self::$IndexTPL->SetParam('PageTitle', "Початкова сторінка");
        $user = Auth::GetUser();
        
        if($user) {
            self::$IndexTPL->SetParam('user', $user);
        }
        
        self::$IndexTPL->SetParams($params);
        self::$IndexTPL->Display();
    }
    public static function ReturnJson($data) {
        if(gettype($data) == "string") {
            echo $data;
        } else {
            echo json_encode($data);
        }        
    }
    public static function Redirect($controller, $method = "", $args = array()) {
        $url = self::GetUrlData();
        
        $location = $url['protocol'] . '://' . $url['host'] . "/" . $controller . "/";
        
        if(!empty($method)) {
            $location = $location . $method . "/";
        }
        
        if(!empty($args)) {
             $location = $location . implode("/", $args);
        }
        
        header("Location: " . $location);
        exit;
    }
    public static function GetControllerName() {
        return self::$controller;
    }
    public static function GetMethodName() {
        return self::$method;
    }
}