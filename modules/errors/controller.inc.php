<?php
class Errors_Controller
{
    public function NotFoundAction($params)
    {
        Core::ReturnView( array(
            "PageTitle" => "404 Не знайдено",
            "Content" => Errors_View::NotFound()
        ));
    }
    public function AccessDeniedAction($params)
    {
        Core::ReturnView( array(
            "PageTitle" => "403 Доступ заборонено",
            "Content" => Errors_View::AccessDenied()
        ));
    }
}