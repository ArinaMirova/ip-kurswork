<?php
class Errors_View
{
    public static function NotFound()
    {
        $tpl = new Template('template/errors/notfound.tpl');
        return $tpl->GetHTML();
    }
    
    public static function AccessDenied()
    {
        $tpl = new Template('template/errors/accessdenied.tpl');
        return $tpl->GetHTML();
    }
}