<?php
class Messages_Controller
{
    public function IndexAction() {
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        $conversations = Messages_Model::GetConversations(Auth::GetUser()->id);
        
        foreach($conversations as $conv) {
            $messages = Messages_Model::GetMessages($conv->id);
            
            $conv->AddMessage(end($messages));
        }
        
        Core::ReturnView( array(
            "PageTitle" => "Повідомлення",
            "Content" => Messages_View::Index($conversations)
        ));
    }
    public function ConvAction($params) {
        $conv = Messages_Model::GetConversation($params['0']);
        $messages = Messages_Model::GetMessages($conv->id);
        
        Core::ReturnView( array(
            "PageTitle" => "Повідомлення",
            "Content" => Messages_View::Conv($conv, $messages)
        ));
    }
    public function StartAction() {
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $row = $_POST;
            
            $id = Messages_Model::StartConv(Auth::GetUser()->id, $row['id']);
            
            Core::Redirect('messages', 'conv', [$id]);
        }
    }
    public function AddAction() {
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $row = $_POST;
            
            if(Auth::GetUser()->id != $row['author_id']) Core::Redirect('errors', 'AccessDenied');
            
            Messages_Model::AddMessage($row['conversation_id'], $row['author_id'], $row['message']);
        }
    }
    public function GetAction() {
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $row = $_POST;
            
            $messages = Messages_Model::GetMessages($row['conversation_id']);
            
            Core::ReturnJson($messages);
        }
    }
}