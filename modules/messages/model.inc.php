<?php
class Messages_Model
{
    public static function GetConversations($id) {
        $data1 = Core::$Db->SelectAll('conversation', 'id', ['fUser_id' => $id]);
                
        $data2 = Core::$Db->SelectAll('conversation', 'id', ['sUser_id' => $id]);
        
        $data = array_merge($data1, $data2);
        
        $res = array();
        
        foreach($data as $id) {
            $id = $id['id'];
            
            $conv = self::GetConversation($id);
            
            array_push($res, $conv);
        }
        
        return $res;
    }
    public static function GetConversation($id) {
        $data = Core::$Db->Select('conversation', '*', ['id' => $id]);
        
        if(!$data) return false;
        
        $conv = new Conversation($data);
        
        if(Auth::GetUser()->id == $data['sUser_id']) {
            $user = Peoples_Model::Get($data['fUser_id']);
            
            $conv->AddUser($user);
        } else if(Auth::GetUser()->id == $data['fUser_id']) {
            $user = Peoples_Model::Get($data['sUser_id']);
            
            $conv->AddUser($user); 
        } else {
            return false;
        }
        
        return $conv;
    }
    public static function GetMessages($id) {
        $data = Core::$Db->SelectAll('message', '*', ['conversation_id' => $id]);
        
        $res = array();
        
        foreach($data as $row) {
            array_push($res, new Message($row));
        }
        
        return $res;
    }
    public static function StartConv($fId, $sId) {
        $data1 = Core::$Db->Select('conversation', 'id', ['fUser_id' => $fId, 'sUser_id' => $sId]);
        $data2 = Core::$Db->Select('conversation', 'id', ['fUser_id' => $sId, 'sUser_id' => $fId]);
                
        if($data1) {
            return $data1['id'];
        } else if ($data2) {
            return $data2['id'];
        }
        
        Core::$Db->Insert('conversation', ['fUser_id' => $fId, 'sUser_id' => $sId]);    
        
        $data = Core::$Db->Select('conversation', 'id', ['fUser_id' => $fId, 'sUser_id' => $sId]);    
        
        return $data['id'];
    }
    public static function AddMessage($convId, $authId, $msg) {
        Core::$Db->Insert('message', ['conversation_id' => $convId, 'author_id' => $authId, 'message' => $msg]);
    }
}