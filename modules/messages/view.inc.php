<?php
class Messages_View
{
    public static function Index($data)
    {
        $tpl = new Template('template/messages/index.tpl');
        $tpl->SetParam('conversations', $data);
        return $tpl->GetHTML();
    }
    public static function Conv($conv, $messages)
    {
        $tpl = new Template('template/messages/conv.tpl');
        $tpl->SetParam('conv', $conv);
        $tpl->SetParam('messages', $messages);
        return $tpl->GetHTML();
    }
}