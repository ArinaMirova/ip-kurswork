<?php
class News_Controller
{
    public function IndexAction($params)
    {
        $news = News_Model::GetAll();
        
        Core::ReturnView( array(
            "PageTitle" => "Сторінка новин",
            "Content" => News_View::Index($news)
        ));
    }
    public function DeleteAction($params)
    {        
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $row = $_POST;
            
            $news = News_Model::Get($row['id']);
            
            if(!$news) {                
                Core::Redirect('errors', 'NotFound');                
            }
            
            if($news->author->id != Auth::GetUser()->id) {                
                Core::Redirect('errors', 'AccessDenied');
            }
            
            News_Model::DeleteById($news->id);
            
            Core::Redirect('peoples', 'info', [$row['author_id']]);
        }
    }    
    public function EditAction($params)
    {
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $row = $_POST;
            
            $row['author_id'] = Auth::GetUser()->id;
            $row['isEdited'] = 1;
            
            News_Model::Update($row);
            
            Core::Redirect('peoples', 'info', [$row['author_id']]);
        }
        
        $news = News_Model::Get($params[0]);
        
        if(!$news) {                
            Core::Redirect('errors', 'NotFound');                
        }

        if($news->author->id != Auth::GetUser()->id) {
            Core::Redirect('errors', 'AccessDenied');
        }
        
        Core::ReturnView( array(
            "PageTitle" => "Сторінка редагування новини",
            "Content" => News_View::Edit($news)
        ));
    }
    public function AddAction()
    {
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $row = $_POST;
            
            $row['author_id'] = Auth::GetUser()->id;
            
            News_Model::Insert($row);
            
            Core::Redirect('peoples', 'info', [$row['author_id']]);
        }
        
        Core::ReturnView( array(
            "PageTitle" => "Сторінка додавання новини",
            "Content" => News_View::Add()
        ));
    }
}