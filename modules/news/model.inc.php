<?php
class News_Model
{
    public static function DeleteById($id)
    {
        Core::$Db->DeleteById('news', 'id', $id);
    }
    public static function Insert($row)
    {
        Core::$Db->Insert('news', $row);
    }
    public static function Update($row)
    {
        Core::$Db->UpdateById('news', $row, 'id', $row['id']);
    }
    public static function Get($id)
    {
        $data = Core::$Db->Select('news', '*', ['id' => $id]);
        
        if(!$data) return false;
        
        $authorData = Core::$Db->Select('user', '*', ['id' => $data['author_id']]);
        
        $data['author'] = new User($authorData);
        
        return new News($data);
    }
    public static function GetAll()
    {
        $data = Core::$Db->SelectAll('news', 'id');
        
        $result = array();
        
        foreach($data as $id) {
            $news = self::Get($id['id']);
            
            if($news) {
                array_push($result, $news);
            }
        }
        
        return $result;
    }
    public static function GetAllByAuthor($id)
    {
        $data = Core::$Db->SelectAll('news', 'id', ['author_id' => $id]);
        
        $result = array();
        
        foreach($data as $id) {
            $news = self::Get($id['id']);
            
            if($news) {
                array_push($result, $news);
            }
        }
        
        return $result;
    }
}