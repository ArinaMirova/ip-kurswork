<?php
class News_View
{
    public static function Index($news)
    {
        $tpl = new Template('template/news/index.tpl');
        $tpl->SetParam('news', $news);
        return $tpl->GetHTML();
    }
    public static function Edit($news)
    {
        $tpl = new Template('template/news/edit.tpl');
        $tpl->SetParam('news', $news);
        return $tpl->GetHTML();
    }
    public static function Add()
    {
        $tpl = new Template('template/news/add.tpl');
        return $tpl->GetHTML();
    }
}