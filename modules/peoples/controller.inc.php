<?php
class Peoples_Controller
{
    public function IndexAction()
    {
        $users = Peoples_Model::GetAll();
        
        Core::ReturnView( array(
            "PageTitle" => "Сторінка новин",
            "Content" => Peoples_View::Index($users)
        ));
    }
    public function EditAction($params)
    {
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $row = $_POST;
            
            $user = Peoples_Model::Get($row['user_id']);
            
            if(!$user) {                
                Core::Redirect('errors', 'NotFound');             
            }
            
            if($user->id != Auth::GetUser()->id) {                
                Core::Redirect('errors', 'AccessDenied');
            }
            
            if(!isset($user->info->id)) {
                Peoples_Model::AddDefault($row['user_id']);
                
                $user = Peoples_Model::Get($row['user_id']);
                
                $row['id'] = $user->info->id;
            }
            
            Peoples_Model::Update($row);
            
            Core::Redirect('peoples', 'info', [$row['user_id']]);
        }
        
        $user = Peoples_Model::Get($params[0]);
        
        Core::ReturnView( array(
            "PageTitle" => "Сторінка новин",
            "Content" => Peoples_View::Edit($user)
        ));
    }
    public function InfoAction($params)
    {        
        if(!Auth::IsAuthorized()) Core::Redirect('errors', 'AccessDenied');
        
        $user = Peoples_Model::Get($params[0]);
        $news = News_Model::GetAllByAuthor($params[0]);
        
        if(!$user) {
            Core::Redirect('errors', 'NotFound');
        }
        
        Core::ReturnView( array(
            "PageTitle" => $user->name.' '.$user->surname,
            "Content" => Peoples_View::Info($user, $news)
        ));
    }
}