<?php
class Peoples_Model
{
    public static function Get($id)
    {
        $data = Core::$Db->Select('user', '*', ['id' => $id]);
        
        if(!$data) return false;
        
        $info = Core::$Db->Select('userinfo', '*', ['user_id' => $id]);
        
        $res = new User($data);
        $res->AddInfo($info);
        
        return $res;
    }
    public static function Update($row)
    {
        Core::$Db->UpdateById('userinfo', $row, 'id', $row['id']);
    }
    public static function AddDefault($id)
    {
        Core::$Db->Insert('userinfo', ['biography' => '', 'intereses' => '', 'adress' => '', 'birthday' => '', 'user_id' => $id]);
    }
    public static function GetAll()
    {
        $data = Core::$Db->SelectAll('user', 'id');
        
        $result = array();
        
        foreach($data as $id) {
            $user = self::Get($id['id']);
            
            
            if($user) {
                array_push($result, $user);
            }
        }
        
        return $result;
    }
}