<?php
class Peoples_View
{
    public static function Index($users)
    {
        $tpl = new Template('template/peoples/index.tpl');
        $tpl->SetParam('users', $users);
        return $tpl->GetHTML();
    }
    public static function Info($user, $news)
    {
        $tpl = new Template('template/peoples/info.tpl');
        $tpl->SetParam('user', $user);
        $tpl->SetParam('news', $news);
        return $tpl->GetHTML();
    }
    public static function Edit($user)
    {
        $tpl = new Template('template/peoples/edit.tpl');
        $tpl->SetParam('user', $user);
        return $tpl->GetHTML();
    }
}