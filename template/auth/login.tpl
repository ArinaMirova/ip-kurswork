<div id="auth" class="login">
    <?php if(!Auth::isAuthorized()): ?>
    <div class="login-info">
        Для входу на сайт заповніть будь-ласка дані поля: 
    </div>
    <form method="post" action="#">
        <table>
            <tr>
                <td class="title">Пошта</td>
                <td class="value">
                    <input class="main" type="email" name="email" placeholder="Введіть вашу пошту"/>
                </td>
            </tr>
            <tr>
                <td class="title">Пароль</td>
                <td class="value">
                    <input class="main" type="password" name="password" placeholder="Введіть ваш пароль"/>
                </td>
            </tr>
        </table>
        
        <input class="button" type="submit" value="Увійти" />
    </form>
    <a class="button small register" href="/auth/register">Зареєструвати новий аккаунт</a>
    <?php else: ?>
    <div>Ви вже увійшли в свій аккаунт!</div>
    <?php endif; ?>
</div>