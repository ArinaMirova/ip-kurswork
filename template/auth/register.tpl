<div id="auth">
    <?php if(!Auth::isAuthorized()): ?>
    <div class="login-info">
        Реєстрація нового користувача: 
    </div>
    <form method="post" action="#">
        <table>
            <tr>
                <td class="title"><label for="register-email">Пошта</label></td>
                <td class="input"><input class="main" id="register-email" type="email" name="email" placeholder="Введіть вашу пошту" required="required" /></td>
            </tr>
            <tr>
                <td class="title"><label for="register-password">Пароль:</label></td>
                <td class="input"><input class="main" type="password" id="register-password" name="password" placeholder="Введіть пароль" required="required" /></td>
            </tr>
            <tr>
                <td class="title"><label for="register-name">Ім'я</label></td>
                <td class="input"><input class="main" type="text" id="register-name" name="name" placeholder="Введіть ваше ім'я" required="required" /></td>
            </tr>
            <tr>
                <td class="title"><label for="register-surname">Фамілія</label></td>
                <td class="input"><input class="main" type="text" id="register-surname" name="surname" placeholder="Введіть вашу фамілію" required="required" /></td>
            </tr>
        </table>
        
        <input type="submit" class="button" value="Зареєструватися"/>
    </form>
    <?php else: ?>
    <div>Ви вже увійшли в свій аккаунт!</div>
    <?php endif; ?>
</div>