<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=$PageTitle ?></title>
    <link rel="stylesheet" href="/content/css/main.css" />
    <?php
        $link = 'content/css/'.Core::GetControllerName().'/main.css';
        if(is_file($link)): 
    ?>
        <link rel="stylesheet" href="/<?=$link?>" />
    <?php endif; ?>
    <?php
        $link = 'content/css/'.Core::GetControllerName().'/'.Core::GetMethodName().'.css';
        if(is_file($link)): 
    ?>
        <link rel="stylesheet" href="/<?=$link?>" />
    <?php endif; ?>
    <script src="/content/js/jquery-3.2.1.min.js"></script>
</head>
<body>
    <header id="site-header">
        <a class="site-logo" alt="Головна сторінка" href="/news/"></a>
        <div class="site-user-menu">
            <?php if(Auth::IsAuthorized()): ?>
            <span class="site-user-menu-hello">
                Привіт, <a class="site-user-menu-link" href="/peoples/info/<?=$user->id?>"><?=$user->name?> <?=$user->surname?></a>
            </span>
            <a class="button logout" href="/auth/logout">Вийти</a>
            <?php else: ?>
            <a class="button login" href="/auth/login">Увійти</a>
            <?php endif; ?>
        </div>
    </header>
    <div id="content"><?=$Content ?></div>
    <?php if(Auth::IsAuthorized()): ?>
    <div id="navigation">
        <ul class="nav-list">
            <li class="nav-button news" title="Новини">
                <a class="nav-link" href="/news"></a>
            </li>
            <li class="nav-button peoples" title="Люди та друзі">
                <a class="nav-link" href="/peoples"></a>
            </li>
            <li class="nav-button messages" title="Повідомлення">
                <a class="nav-link" href="/messages"></a>
            </li>
            <li class="nav-button photos disabled" title="На даний момент недоступно">
                <a class="nav-link"></a>
            </li>
        </ul>
        <div class="nav-button-expand"></div>
    </div>
    <?php endif; ?>
</body>
</html>