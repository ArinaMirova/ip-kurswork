<?php
    if(!Auth::IsAuthorized()) {
        Core::Redirect('errors', 'AccessDenied');
    }

    $user = Auth::GetUser();
?>

<div id="messages">
    <div id="messages-list">        
        <?php foreach($messages as $msg): ?>
        <div class="message <?=($user->id == $msg->author_id ? 'right' : 'left')?>">
            <div class="text"><?=$msg->message?></div>
            <div class="time-day"><?=date('Y-m-d', strtotime($msg->date))?></div>
            <div class="time-hour"><?=date('H:m', strtotime($msg->date))?></div>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="info">
        <div class="user"><?=$conv->user->name?> <?=$conv->user->surname?></div>
        <div class="you"><?=$user->name?> <?=$user->surname?></div>
        <form id="ms-sent" method="post">
            <input type="text" hidden="hidden" name="conversation_id" value="<?=$conv->id?>" />
            <input id="author" type="text" hidden="hidden" name="author_id" value="<?=$user->id?>" />
            
            <textarea class="message main" name="message" placeholder="Впишіть ваше повідомлення..."></textarea>
            
            <input class="button" type="submit" value="Надіслати" />
        </form>
    </div>
</div>
<script src="/content/js/msg.js"></script>