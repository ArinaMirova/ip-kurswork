<?php
    $user = Auth::GetUser();
?>

<div id="messages">
    <div class="title">Повідомлення: </div>
    <div class="conversations">
        <?php foreach($conversations as $data): ?>
        <div class="conversation">
            <div class="user">Діалог з <span class="name"><?=$data->user->name?> <?=$data->user->surname?></span></div>
            <div class="message">
                <span class="icon"><?=($user->id == $data->message->author_id ? '+>' : '<+ ')?></span>
                <div class="text"><?=$data->message->message?></div>
            </div>
            
            <a href="/messages/conv/<?=$data->id?>" class="overlay-link"></a>            
        </div>
        <?php endforeach; ?>
    </div>
</div>