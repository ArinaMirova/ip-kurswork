<div id="news">
    <div class="main-title">
        Заповніть поля нижче щоб додати новину: 
    </div>
    <form method="post" action="">        
        <table>
            <tr>
                <td class="title"><label for="news-title">Назва новини</label></td>
                <td class="input"><input class="main" type="text" id="news-title" name="title" placeholder="Введіть назву новини..." /></td>
            </tr>
            <tr>
                <td class="title"><label for="news-content">Контент</label></td>
                <td class="input"><textarea class="main" id="news-content" name="content" placeholder="Введіть контент новини..."></textarea></td>
            </tr>
        </table>
        
        <input class="button" type="submit" value="Додати" />
    </form>
</div>