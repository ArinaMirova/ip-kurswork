<div id="news">
    <form method="post" action="">
        <input type="text" name="id" value="<?=$news->id?>" hidden="hidden" />
        <input type="text" name="author_id" value="<?=$news->author->id?>" hidden="hidden" />
        
        <table>
            <tr>
                <td class="title"><label for="news-title">Назва новини</label></td>
                <td class="input"><input class="main" type="text" id="news-title" value="<?=$news->title?>" name="title" placeholder="Введіть назву новини..." /></td>
            </tr>
            <tr>
                <td class="title"><label for="news-content">Контент</label></td>
                <td class="input"><textarea class="main" id="news-content" name="content" placeholder="Введіть контент новини..."><?=$news->content?></textarea></td>
            </tr>
        </table>
        
        <input class="button" type="submit" value="Зберегти зміни" />
    </form>
</div>