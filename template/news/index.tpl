<?php if(Auth::IsAuthorized()): ?>
<div class="buttons">
    <a class="button news-new" href="/news/add">Додати новину</a>
</div>
<?php endif; ?>

<div id="news">    
    <?php if(empty($news)):?>
    <div class="notife">Жодної новини ще немає!</div>
    <?php endif; ?>
    <?php foreach($news as $data): ?>
    <div class="news">
        <div class="news-header">
            <div class="title"><?=$data->title?></div>
            <?php if(Auth::IsAuthorized() && Auth::GetUser()->id == $data->author->id): ?>
            <div class="control">
                <a class="button small news-edit" href="/news/edit/<?=$data->id?>">Редагувати новину</a>
                <form method="post" action="/news/delete">
                    <input type="text" name="id" value="<?=$data->id?>" hidden="hidden"/>
                    <button type="submit" class="button small news-remove">Видалити новину</button>
                </form>
            </div>
            <?php endif; ?>
        </div>
        <div class="content"><?=$data->content?></div>
        <div class="news-body">
            <div class="author">Автор: <a href="/peoples/info/<?=$data->author->id?>"><?=$data->author->name.' '.$data->author->surname?></a>
            </div>
            <div class="date"><?=$data->date?></div>
            <?php if($data->isEdited): ?>
            <div class="edited">(відредаговано)</div>
            <?php endif; ?>
        </div>
    </div>
    <?php endforeach; ?>
</div>