<div id="user">
    <div class="name"><?=$user->name?> <?=$user->surname?></div>
    <form method="post" action="">
        <input type="text" hidden="hidden" name="id" value="<?=$user->info->id?>"/>
        <input type="text" hidden="hidden" name="user_id" value="<?=$user->id?>"/>
        
        <table>
            <tr>
                <td>                    
                    <label for="biography">Трішки про себе:</label>
                </td>
                <td>
                    <textarea class="main" id="biography" type="text" name="biography" ><?=$user->info->biography?></textarea>
                </td>
            </tr>
            <tr>
                <td>                    
                    <label for="intereses">Ваші інтереси:</label>
                </td>
                <td>
                    <textarea class="main" id="intereses" type="text" name="intereses" ><?=$user->info->intereses?></textarea>
                </td>
            </tr>
            <tr>
                <td>                    
                    <label for="adress">Ваша адреса:</label>
                </td>
                <td>
                    <input class="main" id="adress" type="text" name="adress" value="<?=$user->info->adress?>" />
                </td>
            </tr>
            <tr>
                <td>                    
                    <label for="birthday">День народження:</label>
                </td>
                <td>
                    <input class="main" id="birthday" type="date" name="birthday" value="<?=date('Y-m-d', strtotime($user->info->birthday))?>" />
                </td>
            </tr>
        </table>
        
        <input class="button" type="submit" value="Оновити" />
    </form>
</div>