<div id="peoples">
    <div class="title">Зареєстровані користувачі:</div>
    <div class="peoples-list">
        <?php foreach($users as $user): ?>
        <div class="people">
            <div class="name">
                <a href="/peoples/info/<?=$user->id?>"><?=$user->name?> <?=$user->surname?></a>
            </div>
            <div class="adress"><?=$user->info->adress?></div>
            <div class="birthday"><?=date('d-m-Y', strtotime($user->info->birthday))?></div>
            
            <a class="overflow-link" href="/peoples/info/<?=$user->id?>"></a>
        </div>
        <?php endforeach; ?>
    </div>    
</div>