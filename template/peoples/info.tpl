<div id="peoples">
    <div class="info">
        <div class="name"><?=$user->name?> <?=$user->surname?></div>
        <div class="biography"><span class="title">Біографія: </span><span class="bg"><?=$user->info->biography?></span></div>
        <div class="intereses"><span class="title">Інтереси: </span><span class="int"><?=$user->info->intereses?></span></div>
        <div class="adress"><span class="title">Адреса: </span><span class="adr"><?=$user->info->adress?></span></div>
        <div class="birthday"><span class="title">День народження: </span><span class="bd"><?=date('d-m-Y', strtotime($user->info->birthday))?></span></div>
    </div>
    <div class="control">
    <?php if(Auth::IsAuthorized() && Auth::GetUser()->id == $user->id): ?>
        <a class="button" href="/news/add">Додати новину</a>
        <a class="button" href="/peoples/edit/<?=$user->id?>">Редагувати профіль</a>
    <?php else: ?>
        <form action="/messages/start" method="post">
            <input hidden="hidden" type="text" value="<?=$user->id?>" name="id"/>
            <input class="button" type="submit" value="Написати повідомлення" />
        </form>
    <?php endif; ?>
    </div>
    <div class="news-list">
        <?php foreach($news as $data): ?>
        <div class="news">
            <div class="news-header">
                <div class="title"><?=$data->title?></div>
                <?php if(Auth::IsAuthorized() && Auth::GetUser()->id == $data->author->id): ?>
                <div class="control">
                    <a class="button small news-edit" href="/news/edit/<?=$data->id?>">Редагувати новину</a>
                    <form method="post" action="/news/delete">
                        <input type="text" name="id" value="<?=$data->id?>" hidden="hidden"/>
                        <button type="submit" class="button small news-remove">Видалити новину</button>
                    </form>
                </div>
                <?php endif; ?>
            </div>
            <div class="content"><?=$data->content?></div>
            <div class="news-body">
                <div class="author">Автор: <a href="/peoples/info/<?=$data->author->id?>"><?=$data->author->name.' '.$data->author->surname?></a>
                </div>
                <div class="date"><?=$data->date?></div>
                <?php if($data->isEdited): ?>
                <div class="edited">(відредаговано)</div>
                <?php endif; ?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>